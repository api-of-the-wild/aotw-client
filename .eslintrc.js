module.exports = {
  env: {
    es6: true,
    node: true,
    mocha: true,
    browser: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:prettier/recommended",
  ],
  globals: {
    bench: true,
  },
  plugins: ["json", "react"],
  rules: {
    eqeqeq: ["error", "always", { null: "ignore" }],
  },
  parser: "babel-eslint",
  settings: {
    react: {
      version: "detect",
    },
  },
};
