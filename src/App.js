import React, { Component } from "react";
import { RedocStandalone } from "redoc";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import "./App.css";

const handleError = error => {
  console.log(error); // eslint-disable-line
};

class App extends Component {
  render() {
    return (
      <div>
        <Tabs>
          <TabList>
            <Tab>Geography</Tab>
            <Tab>Compendium</Tab>
          </TabList>

          <TabPanel>
            <RedocStandalone
              spec={process.env.PUBLIC_URL + "/api/geography.openapi.yaml"}
              onLoaded={handleError}
            />
          </TabPanel>
          <TabPanel>
            <RedocStandalone
              spec={process.env.PUBLIC_URL + "/api/compendium.openapi.yaml"}
              onLoaded={handleError}
            />
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

export default App;
